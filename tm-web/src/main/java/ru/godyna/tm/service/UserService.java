package ru.godyna.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.godyna.tm.api.repository.IUserRepository;
import ru.godyna.tm.enumerated.RoleType;
import ru.godyna.tm.exceprion.field.LoginEmptyException;
import ru.godyna.tm.exceprion.field.PasswordEmptyException;
import ru.godyna.tm.exceprion.user.ExistsLoginException;
import ru.godyna.tm.exceprion.user.PermissionException;
import ru.godyna.tm.exceprion.user.RoleEmptyException;
import ru.godyna.tm.model.Role;
import ru.godyna.tm.model.User;

import javax.transaction.Transactional;
import java.util.Collections;

@Service
public class UserService {

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable RoleType roleType) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (userRepository.existsByLogin(login)) throw new ExistsLoginException();
        if (roleType == null) throw new RoleEmptyException();
        @NotNull final String passwordHash = passwordEncoder.encode(password);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        @NotNull final Role role = new Role(roleType);
        role.setUser(user);
        user.setRoles(Collections.singletonList(role));
        userRepository.saveAndFlush(user);
    }

    @Transactional
    public void save(@Nullable final User user) {
        if (user == null) throw new PermissionException();
        userRepository.saveAndFlush(user);
    }

    @Nullable
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.findByLogin(login);
    }

    public boolean existsByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return userRepository.existsByLogin(login);
    }

}
