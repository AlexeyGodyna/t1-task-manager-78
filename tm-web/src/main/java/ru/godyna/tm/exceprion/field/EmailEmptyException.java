package ru.godyna.tm.exceprion.field;

public final class EmailEmptyException extends AbsractFieldException {

    public EmailEmptyException() {
        super("Error! Email is empty...");
    }

}
